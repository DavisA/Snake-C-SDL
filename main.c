#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>  //for sleep

#include<SDL.h>
#include<SDL2_gfxPrimitives.h>

#include "game.h"



int main(int argc, char ** argv)
{
    if (SDL_Init(SDL_INIT_VIDEO) != 0){
        fprintf(stderr, "Could not start SDL2: %s\n", SDL_GetError());
        return EXIT_FAILURE;
    }

    SDL_Window *window = SDL_CreateWindow("SNAKE!", 100, 100, 
                                            SCREEN_WIDTH, SCREEN_HEIGHT, 
                                            SDL_WINDOW_SHOWN);
    
    if (window == NULL){
        fprintf(stderr, "Could not create Window: %s\n", SDL_GetError());
        return EXIT_FAILURE;
    }

    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, 
                                                SDL_RENDERER_ACCELERATED | 
                                                SDL_RENDERER_PRESENTVSYNC);

     if (renderer == NULL){
         SDL_DestroyWindow(window);
         fprintf(stderr, "Could not create Renderer: %s\n", SDL_GetError());
         return EXIT_FAILURE;
    }


    game_t game;
    game.state = RUNNING;
    reset_game(&game);
    int input = NONE;


    SDL_Event e;



    while(game.state){
        while(SDL_PollEvent(&e)){
            switch(e.type){
                case SDL_QUIT:
                    game.state = 0;
                    break;
 
 
                /* handle input */
                case SDL_KEYDOWN:

                    if(e.key.keysym.sym ==SDLK_SPACE){
                        if(game.state == PAUSE){game.state = RUNNING;}
                        else game.state = PAUSE;
                    }

                    if(e.key.keysym.sym == SDLK_w) {
                        input = UP;
                        break;
                    }
                    if(e.key.keysym.sym == SDLK_a) {
                        input = LEFT;
                        break;
                    }
                    if(e.key.keysym.sym == SDLK_d) {
                        input = RIGHT;
                        break;
                    }
                    if(e.key.keysym.sym == SDLK_s) {
                        input = DOWN;
                        break;
                    }

                default: {}

            }

        }

        if(game.state == RUNNING){
            handleInput(&game.snake, input);
            update(&game);
        }
        

        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);
        render_game(renderer, &game);
        SDL_RenderPresent(renderer);

        /* shtty speed system. */  
        usleep(100000); //sleep a 10th of a second)

    }

    SDL_DestroyWindow(window);
    SDL_Quit();
    return EXIT_SUCCESS; 
}
