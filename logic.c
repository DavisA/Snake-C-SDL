#include <stdbool.h>
#include "game.h"

point2d_t gen_point2d(){
    point2d_t result;
    result.x = rand() % N;
    result.y = rand() % N;
    
    return result;

}

void handleInput(snake_t *snake, const int input){
    
    if(input == RIGHT && snake->delta_x == 0){
        snake->delta_x = 1;
        snake->delta_y = 0;
    } else if (input == LEFT && snake->delta_x == 0){
        snake->delta_x = -1;
        snake->delta_y = 0;
    } else if (input == UP && snake->delta_y == 0){
        snake->delta_x = 0;
        snake->delta_y = -1;
    } else if (input == DOWN && snake->delta_y == 0){
        snake->delta_x = 0;
        snake->delta_y = 1;
    }    

}

void reset_snake(snake_t *snake){
    snake->size = 0;
    snake->head.x = 1;
    snake->head.y = 1;   
    
    snake->tail[snake->size] = snake->head;
    snake->delta_x = 1;
    snake->delta_y = 0;


}

void reset_game(game_t *game){

    game->state = PAUSE;
    reset_snake(&game->snake);   
    game->food = gen_point2d();
}

void update_snake(snake_t *snake){
    snake->head.x += snake->delta_x;
    snake->head.y += snake->delta_y;

    for(int i = 0; i < snake->size; i++){
        snake->tail[i] = snake->tail[i+1];
    }

    snake->tail[snake->size] = snake->head;



    if (snake->head.x > N-1){snake->head.x = N-1;}
    if (snake->head.y > N-1){snake->head.y = N-1;}
    if (snake->head.x < 0){snake->head.x = 0;}
    if (snake->head.y < 0){snake->head.y = 0;}

}

int point2d_collision(point2d_t a, point2d_t b){
    if ((a.x == b.x) && (a.y == b.y)){
        return 1;
    } 
    return 0;
}

void grow_snake(snake_t *snake){
    snake->size++;
    snake->tail[snake->size] = snake->head;
}

void check_tail_collision(game_t *game){
    for(int i = 0; i < game->snake.size -1; i++){
        if(point2d_collision(game->snake.head, game->snake.tail[i])){
            reset_game(game);
            break;
        }
    }
}

void gen_food(game_t *game){
    int bad_food = 0;

    do{
        bad_food = 0;
        game->food = gen_point2d();
        for(int i = 0; i <= game->snake.size; i++){
            if(point2d_collision(game->food, game->snake.tail[i])){
                bad_food = 1;
                break;
            }
        }


    } while(bad_food);
}

void update(game_t *game){
    update_snake(&game->snake);

    if(point2d_collision(game->snake.head, game->food)){
        grow_snake(&game->snake);
        gen_food(game);   
    }

    check_tail_collision(game);

}