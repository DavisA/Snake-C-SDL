#ifndef GAME_H_
#define GAME_H_

#include<SDL.h>
#include<SDL2_gfxPrimitives.h>

#define N 30
#define SCREEN_WIDTH 750
#define SCREEN_HEIGHT 750
#define BLOCK_HEIGHT SCREEN_HEIGHT / N
#define BLOCK_WIDTH SCREEN_WIDTH / N


//GAME STATES
#define EXIT 0
#define RUNNING 1
#define PAUSE 2

//DIRECTIONS
#define NONE 0
#define LEFT 1
#define RIGHT 2
#define DOWN 3
#define UP 4

//HIGHEST POSSIBLE SCORE
#define MAX_TAIL N*N


typedef struct{
    int x;
    int y;
} point2d_t;

typedef struct{
    point2d_t head;
    int size;
    point2d_t tail[MAX_TAIL];
    int delta_x;
    int delta_y;
} snake_t;

typedef struct {
    snake_t snake;
    int state;
    point2d_t food;
} game_t;


void update(game_t *game);
void handleInput(snake_t *snake, int direction);
void reset_game(game_t *game);

void render_game(SDL_Renderer *renderer, const game_t *game);
;



#endif //Game_H_