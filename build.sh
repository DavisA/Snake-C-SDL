#!/bin/bash
if [ ! -d ./bin ]; then
    mkdir bin/;
fi;

#gcc -I /usr/include/SDL2 *.c -lSDL
gcc *.c $(sdl2-config --cflags --libs) -lSDL2_gfx -o bin/main

