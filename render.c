#include<SDL.h>
#include<SDL2_gfxPrimitives.h>
#include "game.h"

const SDL_Color FOOD_COLOR = {.r = 255, .g = 0, . b = 0};
const SDL_Color SNAKE_TAIL_COLOR = {.r = 255, .g = 255, . b = 255};
const SDL_Color SNAKE_HEAD_COLOR = {.r = 135, .g = 238, . b = 238};
const SDL_Color GRID_COLOR = {.r = 100, .g = 100, . b = 100};


/* render a grid */
void render_grid(SDL_Renderer *renderer){
    SDL_SetRenderDrawColor(renderer, GRID_COLOR.r, GRID_COLOR.g, GRID_COLOR.b, 255);

    for(int i = 1; i < N; i++){
        SDL_RenderDrawLine(renderer, i * BLOCK_WIDTH, 0, i * BLOCK_WIDTH, SCREEN_HEIGHT);  //draw vertial lines
    }

    for(int i = 1; i < N; i++){
        SDL_RenderDrawLine(renderer, 0, i * BLOCK_HEIGHT, SCREEN_WIDTH, i * BLOCK_HEIGHT);  //draw vertial lines
    }
}

/* Renders a block on the grid*/
void render_block(SDL_Renderer *renderer, const SDL_Color color, const int x, const int y){
    boxRGBA(renderer, x*BLOCK_WIDTH, y*BLOCK_HEIGHT, (x*BLOCK_WIDTH + BLOCK_WIDTH), (y*BLOCK_HEIGHT + BLOCK_HEIGHT), color.r,color.g,color.b, 255);
}

void render_snake(SDL_Renderer *renderer, const snake_t *snake){
    
    /* render tail */
    for(int i = 0; i <= snake->size; i++){
        render_block(renderer, SNAKE_TAIL_COLOR, snake->tail[i].x, snake->tail[i].y);
    }

    /* render head */
    render_block(renderer, SNAKE_HEAD_COLOR, snake->head.x, snake->head.y);
    

}



void render_game(SDL_Renderer *renderer, const game_t *game){
    

    render_block(renderer, FOOD_COLOR, game->food.x, game->food.y);  
    render_snake(renderer, &game->snake);
    render_grid(renderer);
     
    
}